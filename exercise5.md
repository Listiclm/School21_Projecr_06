## Эндпоинты

## №1
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities  
 
3. Заголовки запроса "Activity"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии)  

```
[
  {
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2023-06-19T16:46:18.933165+00:00",
    "completed": false
  },
  {
    "id": 2,
    "title": "Activity 2",
    "dueDate": "2023-06-19T17:46:18.9331669+00:00",
    "completed": true
  },
  {
    "id": 3,
    "title": "Activity 3",
    "dueDate": "2023-06-19T18:46:18.9331672+00:00",
    "completed": false
  },
  {
    "id": 4,
    "title": "Activity 4",
    "dueDate": "2023-06-19T19:46:18.9331674+00:00",
    "completed": true
  },
  ....
  ]  
  ```
  
  ## №2
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities
 
3. Заголовки запроса "Activity"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-19T16:15:29.524Z",
  "completed": true
}
```
5. Статус-код ответа 200  
6. Тело ответа (при наличии)  

```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-19T16:15:29.524Z",
  "completed": true
}
```

## №3
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/5  
 
3. Заголовки запроса "Activity 5"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии)  

```
{
  "id": 5,
  "title": "Activity 5",
  "dueDate": "2023-06-19T21:34:11.199462+00:00",
  "completed": false
}
```

## №4
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/40  

3. Заголовки запроса "Activity 40"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 404  
6. Тело ответа (при наличии)  

```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-daf2588d1fc39841b1e242ab5ccf35c5-9d6cb1c7c268ef4b-00"
}
```

## №5
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/5  
 
3. Заголовки запроса "Activity 5"  
4. Тело запроса (при наличии)

```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-19T16:25:12.613Z",
  "completed": true
}
```

5. Статус-код ответа 200  
6. Тело ответа (при наличии)  

```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-19T16:25:12.613Z",
  "completed": true
}
 ```
## №6
1. HTTP-метод DELETE  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/5  

3. Заголовки запроса "Activity 5"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии)  

## №7
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors  

3. Заголовки запроса "Authors"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 

```
[
  {
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  },
  {
    "id": 2,
    "idBook": 1,
    "firstName": "First Name 2",
    "lastName": "Last Name 2"
  },
  {
    "id": 3,
    "idBook": 1,
    "firstName": "First Name 3",
    "lastName": "Last Name 3"
  },
  {
    "id": 4,
    "idBook": 1,
    "firstName": "First Name 4",
    "lastName": "Last Name 4"
  },
....
]
```
## №8
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors  

3. Заголовки запроса "Authors" 
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
## №9
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/book/5  

3. Заголовки запроса "Authors 5"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 

```
[
  {
    "id": 11,
    "idBook": 5,
    "firstName": "First Name 11",
    "lastName": "Last Name 11"
  },
  {
    "id": 12,
    "idBook": 5,
    "firstName": "First Name 12",
    "lastName": "Last Name 12"
  },
  {
    "id": 13,
    "idBook": 5,
    "firstName": "First Name 13",
    "lastName": "Last Name 13"
  }
]
```

## №10
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/7  

3. Заголовки запроса "Authors 7"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 7,
  "idBook": 3,
  "firstName": "First Name 7",
  "lastName": "Last Name 7"
}
```
## №11
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/1000  

3. Заголовки запроса "Authors 1000"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 404  
6. Тело ответа (при наличии) 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-a32a931d5db5644da22d19844f0acbc3-64bfe92d3ec33142-00"
}
```
## №12
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/7  

3. Заголовки запроса "Authors 7"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

## №13
1. HTTP-метод DELETE  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/9  

3. Заголовки запроса "Authors 9"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 

## №14
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books  

3. Заголовки запроса "Books"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
[
  {
    "id": 1,
    "title": "Book 1",
    "description": "Labore hendrerit invidunt aliquyam elitr accusam erat vero suscipit invidunt ut voluptua lobortis at consetetur dolore duo. Amet suscipit dolor et sit nulla voluptua luptatum magna dolore ut sit eirmod. Dolores ipsum aliquyam. Velit est sadipscing dolores consetetur dolore ut diam nonumy. Diam sanctus dolore diam dolor amet consetetur justo gubergren duo in euismod stet gubergren in nonumy exerci. Ipsum at gubergren et tempor delenit nulla erat consetetur. Ipsum sanctus magna vero in et vel at clita takimata praesent tempor et ipsum. Tempor eirmod amet elitr luptatum doming et voluptua amet erat amet consetetur justo justo. Erat diam dignissim dolor possim est.\n",
    "pageCount": 100,
    "excerpt": "Tempor lorem ea delenit voluptua sit tempor est in clita diam. Et dolore invidunt nonummy. Elitr erat consequat sed magna augue voluptua. Justo eirmod sed lorem. Eros sanctus amet in no tempor sit aliquyam aliquyam at. Vero vel kasd diam et ipsum tempor lorem sadipscing dolores dolor at aliquip diam dolore diam. Lobortis erat clita tincidunt lorem. Lorem ut amet lorem vero lorem hendrerit dolor sit est accusam lorem iriure eos ea tempor elitr dolor. Autem sit rebum autem aliquyam sit dolor kasd gubergren at mazim stet magna. Doming clita dolore. Diam eum lorem. Et sit et et justo amet erat facer consequat dolore sadipscing diam justo invidunt. Vero justo lorem. Nonumy magna eu et sadipscing sit nonumy ut clita dolore et ipsum sanctus et ipsum vel clita sed sit. Takimata magna nonumy liber stet in et tincidunt est accusam clita dolor vero accumsan.\nTempor sed sed kasd suscipit dolore nonummy facilisi et dolore eirmod. Et assum erat nonummy et. Dolore takimata kasd vero gubergren et blandit est. Amet possim ea. Magna tempor amet et labore labore zzril lobortis dolore.\nAmet dolore sit sit ipsum luptatum. Feugiat erat duo. No elitr nibh lorem est nihil justo vel sea duo est at eos et sed eos amet clita. Et eirmod et facilisis vero sed laoreet invidunt et dolore takimata vero volutpat ullamcorper takimata imperdiet ipsum. Enim erat ipsum lorem rebum amet rebum amet accumsan sed accusam. Clita amet justo accusam ut tempor nonumy illum liber consetetur sit clita volutpat sit ipsum consetetur ipsum. Ea illum ullamcorper. Ex et sanctus rebum eos.\nRebum dolor id ea sadipscing justo. Lorem consequat accusam justo et at lorem est clita diam voluptua lorem sadipscing tempor dolore. Stet stet ut vel enim invidunt et sanctus. Magna consequat tempor possim lorem aliquyam invidunt veniam et justo molestie ut sit kasd et. Erat et ea. Quod et amet consectetuer labore consectetuer consetetur nam consetetur duis justo vel minim lorem ut dolore vulputate zzril sanctus.\nVel ut magna. Nonumy et sea. Euismod ea ea sit duis dolor ad diam et ullamcorper no consequat. Lorem vero lorem ipsum sit et ipsum. Sit ad ea lorem voluptua vero delenit erat vero consetetur volutpat. Dolores duo commodo lorem illum magna. Kasd ea ea sadipscing dolor sea ipsum ea te kasd et stet et sea aliquyam aliquyam est lorem dolor. Aliquyam dolores kasd rebum duis tation justo. Ut autem eleifend invidunt justo justo elitr sed erat. Justo rebum clita sadipscing lorem duo lorem in takimata no invidunt magna nulla est vulputate. Et sadipscing takimata sit duis est hendrerit eum rebum labore et tempor dolor accusam illum vulputate dolore. Magna luptatum kasd liber rebum sadipscing gubergren et duo gubergren ea. Ipsum gubergren ipsum kasd dolores amet dolores. Ut vel at amet erat lorem et ut invidunt est consetetur aliquyam takimata nonumy et molestie magna. Sed amet sed nibh tempor erat dolor invidunt dolore aliquam. Facer est duo elitr vel ipsum gubergren in. Gubergren kasd magna at veniam consectetuer.\n",
    "publishDate": "2023-06-18T18:04:43.9472048+00:00"
  },
  {
    "id": 2,
    "title": "Book 2",
    "description": "Lorem option invidunt clita congue duis eirmod labore takimata voluptua erat delenit ipsum sadipscing elitr labore nihil dolore. At tempor eos vero facilisis amet justo voluptua dolores stet ea ea lorem diam diam vel magna consequat. Elitr velit magna nulla nostrud et tempor kasd no volutpat dolor. Dolor sit vel at et dolore at at et erat vero et et elitr kasd tincidunt. At erat gubergren nonumy ipsum in eu nostrud ipsum sed aliquip assum labore tation ut ea stet sit duo.\n",
    "pageCount": 200,
    "excerpt": "Consetetur eleifend facilisi est sed stet ea diam sit consequat. Dolore at et eirmod. Consetetur amet sanctus ipsum erat blandit laoreet facilisi diam et vulputate vel. No sed consetetur labore aliquip magna erat qui et no sed no consequat eum in placerat takimata te. Possim aliquam eirmod et kasd. Voluptua diam sed lorem ea clita amet dolor accusam in nonummy dolores ipsum. Ipsum sadipscing facilisi gubergren esse sanctus et nibh sanctus. Sit et ut dolor. Sit esse consequat dolore nulla aliquyam lorem sit qui justo sit aliquip clita minim justo gubergren. Elitr ipsum diam aliquyam aliquyam sed diam wisi minim dolore accusam dolor. Dolore et amet at assum. Magna labore erat sit ipsum placerat eirmod elitr velit gubergren gubergren ea tempor et te diam soluta. Dolor accusam tation sadipscing dolore amet ex dolore velit et.\nPraesent aliquam diam nulla delenit sed sadipscing laoreet sed magna. Dignissim aliquam vero. Imperdiet labore voluptua nonumy elitr iusto. Sed dolores duis eirmod invidunt justo in et sit duo accusam amet sadipscing eos tempor in. In luptatum diam ea elitr eu sanctus eos sea rebum takimata lorem rebum feugiat sed. Laoreet autem gubergren dolor voluptua lorem. Ipsum at esse. Duis et magna feugiat. Clita justo ut consetetur amet autem justo nisl. Ea eum sanctus volutpat amet suscipit accusam dolore sit possim. Diam eirmod ea labore hendrerit erat hendrerit. Liber takimata et nibh ad et sanctus autem dolor labore. Velit et et. Dolore hendrerit duo sit in sit dolore. Et sea ut vel diam ut esse amet et tempor duo dolor labore vero et exerci.\nKasd et diam rebum. Et tincidunt quis qui te diam. Sit diam labore ut duo amet vel. Et et facilisis et et illum et stet. Labore laoreet sed no zzril diam magna nostrud diam duis stet elitr augue justo. Clita amet illum dolor. Lorem quod lorem eros tempor dolore nonumy nonummy clita sea ipsum aliquyam tempor rebum illum eirmod elit feugait magna. Tempor at sea lorem et kasd nisl ipsum qui nisl et lorem sed ipsum ipsum gubergren wisi nobis. Lorem dolores dolor ex dolore consetetur et sanctus sit invidunt eirmod amet. Sit amet magna sea kasd amet in sadipscing sadipscing invidunt voluptua tincidunt. Ipsum magna et lorem sadipscing eirmod sed est diam aliquyam stet diam diam iriure lorem sanctus. Duo magna lorem. Enim sed nonumy diam consetetur blandit vero et kasd magna nulla. Consectetuer vero illum et ea vero. Et et diam accusam sadipscing sit esse eos in odio ut justo.\nSit volutpat sadipscing consetetur no ea justo est at amet dolore. Duo amet tempor lorem labore wisi at. Et sit nulla sea amet facilisi diam sit magna nulla laoreet duis. Et consetetur eirmod diam diam gubergren imperdiet duis et erat eos. Magna labore hendrerit vel sed est et stet dolore et voluptua consetetur. Duis sit sea lobortis at ut. Eirmod ut dolore dolor adipiscing et. Vero sed dolore praesent duo tempor dolor stet sea vel diam nonumy tempor lorem labore. Doming vulputate consetetur aliquam. No lorem sed sea sadipscing sit et sit aliquip et sit elitr ipsum rebum. Dolores sed takimata lobortis dolores ut minim amet aliquyam. Lorem erat est aliquyam eos elit est delenit sit et at diam nostrud ut exerci. Dolores sanctus eos eros dolor sanctus ut dolores stet. Erat est commodo euismod wisi in clita eos nibh et aliquyam laoreet. Sed sit sanctus sit ipsum te voluptua duis sit lorem et duo ut et. Et sadipscing dolor nisl sadipscing dolor suscipit aliquam dolor nonumy ea. Sed amet duis sadipscing vero sed imperdiet dolore dolore cum est tempor sanctus. Est sed et ipsum lobortis no dolore gubergren justo amet sanctus placerat consequat nonummy est iriure dolore.\nClita ipsum clita takimata diam no eros. Diam sed justo nonumy hendrerit et takimata sit sed ipsum exerci eu gubergren sed molestie dolor voluptua invidunt. Diam et te ut. Elitr takimata euismod eirmod sadipscing lorem magna dolor magna duis elit ea ex. Stet in voluptua elit sed dolore assum. Labore et erat facilisis adipiscing lorem voluptua ea sed magna nonumy wisi feugiat invidunt et duo lorem ut et. Amet exerci et dolor sit stet vero sed et tation praesent iusto et erat invidunt amet hendrerit dolore elitr.\n",
    "publishDate": "2023-06-17T18:04:43.9472921+00:00"
  },
  ...
]
```
## №15
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books  

3. Заголовки запроса "Books"  
4. Тело запроса (при наличии) 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-19T18:08:00.505Z"
}
``` 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-19T18:08:00.505Z"
}
```

## №16
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/7  

3. Заголовки запроса "Books 7"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 7,
  "title": "Book 7",
  "description": "Sed ipsum nisl justo at consetetur enim accusam dolore sed duis amet justo tempor. Dolor magna sadipscing in facilisis et vero laoreet kasd ut. Velit accusam vel duo assum dolor eirmod aliquip aliquyam dolores in ut sea ea veniam vero dolor aliquyam eros. Kasd ut amet nulla esse et possim hendrerit amet vulputate consetetur sadipscing ipsum rebum liber sadipscing. Feugiat vulputate ipsum erat ex stet invidunt eirmod. Invidunt at ea iriure. Facilisi labore eirmod facilisis dolor lorem est et feugiat sanctus eu sadipscing exerci sanctus adipiscing magna. Erat est delenit in sed tincidunt sanctus vel et stet diam. Volutpat facilisis tincidunt amet justo amet elitr velit illum dolores. Consequat autem no dolor dolore amet magna facilisis sit rebum in rebum. Dolore praesent commodo nonumy sed dolores magna diam vel. Sit gubergren sadipscing iriure nonumy lorem nulla kasd sed minim et sadipscing. Dolore nibh quod invidunt labore. Vel et aliquyam duo amet blandit blandit invidunt stet sit volutpat invidunt congue ut. Stet ut eirmod dolor illum nibh consetetur odio at dolor at eirmod voluptua dignissim dolor illum. Eu nam sit tempor et. Erat vulputate est justo enim et aliquyam facilisis dolore nulla velit duo et aliquyam duo praesent voluptua at est. Magna duo enim sed consequat kasd dolore ea accusam sed ut dolore ipsum facilisis.\n",
  "pageCount": 700,
  "excerpt": "Rebum tation nulla liber et autem gubergren enim voluptua ut dolore stet option et iriure in et. Quis aliquyam consetetur enim cum ipsum eos. Est amet facilisi sed et takimata dolore. Aliquyam diam dolor dolores justo invidunt in sit cum aliquyam rebum ipsum lorem sanctus aliquyam quis option.\nDiam dolor duis. Sit dolore velit in. Sanctus et eirmod in dolore clita te nulla ipsum labore dolor hendrerit et sadipscing gubergren iusto sed et. Luptatum ea ipsum laoreet labore sadipscing liber ut. Et et dolores lorem ea tation amet assum minim ut. Tation dolore ipsum minim ea vero at vero euismod eirmod amet est diam aliquyam eos sit sea sea. Eirmod vel sadipscing est consetetur duis amet rebum sit duis rebum sed sanctus ipsum. Ipsum et sit tempor veniam diam labore dolore tincidunt praesent et sed vel duo placerat tation. Aliquyam sit takimata vulputate dolores placerat feugait at et kasd clita at enim ea sadipscing sit. Dolore lorem aliquam justo dolor et ea stet sit. Kasd accusam et dolor est invidunt sed diam accumsan sit cum option. Consequat sed sit volutpat voluptua eirmod gubergren stet suscipit voluptua amet ut assum tempor. Dolores elit no sea. Justo sed takimata veniam rebum sea ut. Eirmod duo est sit sit magna invidunt vel et labore. At tincidunt sit accusam.\nNibh ut ipsum dolore consetetur amet tempor. Invidunt erat lorem labore labore cum rebum dolore dolor et est accusam duis labore. Dolor consequat dolor sadipscing amet. Tempor duo sit clita dolore ipsum sanctus at suscipit consetetur accumsan duo ipsum zzril.\nConsequat dolor blandit facilisis lorem invidunt duo wisi eos sadipscing dolore et sit luptatum sit ut ut. Duis sea eros. Dolor duo at duo invidunt lobortis magna ipsum sit tempor est et invidunt magna tincidunt dolore tempor amet amet. Nonumy velit mazim suscipit vel nonumy. Eirmod eos kasd magna duo tincidunt at et dolor sanctus dolor. Eu ut ut dolor sadipscing ea no facilisis sit nonumy labore est lorem kasd kasd dolore. Hendrerit invidunt ex dolor gubergren elitr est ut liber eirmod magna sed dolor. Ea nonumy illum nonumy rebum ut hendrerit labore possim eos sed rebum at rebum sanctus dolore vulputate at. Tempor ullamcorper dignissim minim lorem clita no et clita molestie. Amet amet ipsum eos et et eirmod. Ea lorem aliquip nonummy kasd elit dolor aliquyam eu laoreet stet aliquam consequat lorem tempor kasd illum ad sit. Et voluptua amet erat duis elitr erat erat sit est amet sed.\nMagna consetetur sit et aliquyam magna ut. Id at dolores lorem labore euismod stet nibh et euismod molestie sit dolor gubergren duis dolore magna eos. Mazim et diam adipiscing laoreet lorem dolor sanctus consetetur tincidunt ipsum. Rebum dolor aliquyam dolore dolore adipiscing dolor vero dolore dolore et nibh molestie eirmod dolore dolores. Ea exerci ipsum et commodo sadipscing sed dolores dolore amet consetetur hendrerit et ipsum lorem. Sit vero est et dolore invidunt consetetur commodo amet aliquyam gubergren stet wisi. Duo est diam dolores. Et sit elitr ea tempor eos stet lorem vel dolor nonumy dolor stet. Et stet ea diam consetetur diam et volutpat justo et erat sit tempor sit enim facilisis suscipit minim ut.\n",
  "publishDate": "2023-06-12T18:11:05.5118708+00:00"
}
```
## №17
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/1000  

3. Заголовки запроса "Books 1000"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 404  
6. Тело ответа (при наличии) 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-907208e211f0ad4d92e5fe0cf591a182-0c392c1de83e5f4a-00"
}
```
## №18
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/10  

3. Заголовки запроса "Books 10"  
4. Тело запроса (при наличии)
```
 {
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-19T18:16:42.609Z"
}
```
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-19T18:16:42.609Z"
}
```
## №19
1. HTTP-метод DELETE  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/5  

3. Заголовки запроса "Books 5"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 

## №20
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos  

3. Заголовки запроса "CoverPhotos"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
[
  {
    "id": 1,
    "userName": "User 1",
    "password": "Password1"
  },
  {
    "id": 2,
    "userName": "User 2",
    "password": "Password2"
  },
  {
    "id": 3,
    "userName": "User 3",
    "password": "Password3"
  },
  {
    "id": 4,
    "userName": "User 4",
    "password": "Password4"
  },
  ...
]
```

## №21
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos  

3. Заголовки запроса "CoverPhotos"  
4. Тело запроса (при наличии) 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
## №22
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/19  

3. Заголовки запроса "CoverPhotos 19"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
[
  {
    "id": 19,
    "idBook": 19,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 19&w=250&h=350"
  }
]
```

## №23
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/15 

3. Заголовки запроса "CoverPhotos 15"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 15,
  "idBook": 15,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 15&w=250&h=350"
}
```
## №24
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1000 

3. Заголовки запроса "CoverPhotos 1000"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 404  
6. Тело ответа (при наличии) 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-b233c642347d03469ecde7be3da9f478-67446f8b6ec3a244-00"
}
```
## №25
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/4 

3. Заголовки запроса "CoverPhotos 4"  
4. Тело запроса (при наличии) 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
## №26
1. HTTP-метод DELETE  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1000 

3. Заголовки запроса "CoverPhotos 1000"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 

## №27
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users

3. Заголовки запроса "Users"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
[
  {
    "id": 1,
    "userName": "User 1",
    "password": "Password1"
  },
  {
    "id": 2,
    "userName": "User 2",
    "password": "Password2"
  },
  {
    "id": 3,
    "userName": "User 3",
    "password": "Password3"
  },
  {
    "id": 4,
    "userName": "User 4",
    "password": "Password4"
  },
  ...
]
```
## №28
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users

3. Заголовки запроса "Users"  
4. Тело запроса (при наличии) 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
## №29
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/10

3. Заголовки запроса "Users 10"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 10,
  "userName": "User 10",
  "password": "Password10"
}
```
## №30
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/50

3. Заголовки запроса "Users 50"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 404  
6. Тело ответа (при наличии) 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-27da339272dfc84c826fa0c6fd2a46d0-1bffbf8f26a7a04c-00"
}
```
## №31
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/2

3. Заголовки запроса "Users 2"  
4. Тело запроса (при наличии) 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
5. Статус-код ответа 200  
6. Тело ответа (при наличии) 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
## №32
1. HTTP-метод DELETE  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/13

3. Заголовки запроса "Users 13"  
4. Тело запроса (при наличии) 
5. Статус-код ответа 200  
6. Тело ответа (при наличии)  

## №33
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities
 
3. Заголовки запроса "Activity"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "title": 0,
  "dueDate": "2023-06-19T18:58:38.607Z",
  "completed": true
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  

```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-0c81bfe21064f34a9f9e38cb82d2139c-4f95c34837ec0c46-00",
  "errors": {
    "$.title": [
      "The JSON value could not be converted to System.String. Path: $.title | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}
```

## №34
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/8
 
3. Заголовки запроса "Activity 8"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "title": 0,
  "dueDate": "2023-06-19T19:03:43.986Z",
  "completed": true
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-aa61d6b4be10a1439e51602caae5a348-a33c9fd1525dfe49-00",
  "errors": {
    "$.title": [
      "The JSON value could not be converted to System.String. Path: $.title | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}
```
## №35
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors
 
3. Заголовки запроса "Authors"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "idBook": 0,
  "firstName": 0,
  "lastName": "string"
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-72a25af3d2f4224da9e10aa6b041858c-1083323e9e3dec4b-00",
  "errors": {
    "$.firstName": [
      "The JSON value could not be converted to System.String. Path: $.firstName | LineNumber: 0 | BytePositionInLine: 32."
    ]
  }
}
```
## №36
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/10
3. Заголовки запроса "Authors 10"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "idBook": 0,
  "firstName": 0,
  "lastName": "string"
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-543dec17efe1e543b31c456ca9e4d1a5-d0f19fc7b8871944-00",
  "errors": {
    "$.firstName": [
      "The JSON value could not be converted to System.String. Path: $.firstName | LineNumber: 0 | BytePositionInLine: 32."
    ]
  }
}
```
## №37
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books
3. Заголовки запроса "Books"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "title": 0,
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-19T19:10:56.870Z"
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-ec8c4dc72eeb1a4bb9981dd3b3fced45-daa509351e8d4a47-00",
  "errors": {
    "$.title": [
      "The JSON value could not be converted to System.String. Path: $.title | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}
```
## №38
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/8
3. Заголовки запроса "Books 8"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "title": 0,
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-19T19:13:20.387Z"
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-d9ccc9e99e1e2f4d8536479966c42d2d-90762624c3c8c743-00",
  "errors": {
    "$.title": [
      "The JSON value could not be converted to System.String. Path: $.title | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}
```
## №39
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos
3. Заголовки запроса "CoverPhotos"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "idBook": 0,
  "url": 0
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-9224de724b27a84281996c751422b608-5daf0d3ae3fe1a47-00",
  "errors": {
    "$.url": [
      "The JSON value could not be converted to System.Uri. Path: $.url | LineNumber: 0 | BytePositionInLine: 26."
    ]
  }
}
```
## №40
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/8
3. Заголовки запроса "CoverPhotos 8"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "idBook": 0,
  "url": 0
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-b504919cba478848b6a4e65733d3c908-f15e2ff5c0326149-00",
  "errors": {
    "$.url": [
      "The JSON value could not be converted to System.Uri. Path: $.url | LineNumber: 0 | BytePositionInLine: 26."
    ]
  }
}
```
## №41
1. HTTP-метод POST  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users
3. Заголовки запроса "Users"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "userName": 0,
  "password": "string"
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-4aaae99dc3a147469dc9afc8a0a4258a-1c1686b1f4a52f4a-00",
  "errors": {
    "$.userName": [
      "The JSON value could not be converted to System.String. Path: $.userName | LineNumber: 0 | BytePositionInLine: 20."
    ]
  }
}
```
## №42
1. HTTP-метод PUT  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/8
3. Заголовки запроса "Users"  
4. Тело запроса (при наличии)  
```
{
  "id": 0,
  "userName": 0,
  "password": "string"
}
```
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-e79b44319e170243ab4c82e5cd367383-38518e014125f945-00",
  "errors": {
    "$.userName": [
      "The JSON value could not be converted to System.String. Path: $.userName | LineNumber: 0 | BytePositionInLine: 20."
    ]
  }
}
```
## №43
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/7777777777777777777777777777
3. Заголовки запроса "Authors 7777777777777777777777777777"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-c7b4956a804f1a49b57d8e04b19bbe38-5716aba4206cb846-00",
  "errors": {
    "idBook": [
      "The value '7777777777777777777777777777' is not valid."
    ]
  }
}
```
## №44
1. HTTP-метод GET  
2. Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/7777777777777777777777777777
3. Заголовки запроса "CoverPhotos 7777777777777777777777777777"  
4. Тело запроса (при наличии)  
5. Статус-код ответа 400  
6. Тело ответа (при наличии)  
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-782a1de76991bc46b05142a020b4070d-bf0a995d9aa3b04d-00",
  "errors": {
    "idBook": [
      "The value '7777777777777777777777777777' is not valid."
    ]
  }
}
```